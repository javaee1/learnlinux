w# User Management

## Switching to User
```bash
    $ sudo -i // switching to root user
    $ sudo -{username} // switching to a user
    $ su - {username} // switching to a user
```

## Creating/Deleting a User
```bash
    $ useradd -D // see the default configuration
    $ sudo adduser
    $ sudo useradd -g users -G sudo -m -s bin/bash -C "This is for testing" {username}
    $ passwd {username}
    $ sudo userdel {username}
```

## Understanding /etc/passwd File Format 

![etc-file-format](./img/etc-passwd-file-format.png) 

**From the above image:**

1. `Username:` It is used when user logs in. It should be between 1 and 32 characters in length.

2. `Password:` An x character indicates that encrypted password is stored in /etc/shadow file. Please note that you need to use the passwd command to computes the hash of a password typed at the CLI or to store/update the hash of the password in /etc/shadow file.

3. `User ID (UID):` Each user must be assigned a user ID (UID). UID 0 (zero) is reserved for root and UIDs 1-99 are reserved for other predefined accounts. Further UID 100-999 are reserved by system for administrative and system accounts/groups.

4. `Group ID (GID):` The primary group ID (stored in /etc/group file)

5. `User ID Info:` The comment field. It allow you to add extra information about the users such as user’s full name, phone number etc. This field use by finger command.

6. `Home directory:` The absolute path to the directory the user will be in when they log in. If this directory does not exists then users directory becomes /

7. `Command/shell:` The absolute path of a command or shell (/bin/bash). Typically, this is a shell. Please note that it does not have to be a shell.

**To search for a username called tom, use the grep command:**
```bash
    $ grep {username} /etc/passwd
    $ grep -w '^{username}' /etc/passwd
```

## Understanding /etc/shadow File Format 

![etc-shadow-file](./img/etc-shadow-file-format.png)

**From the above image:**

1. `Username :` It is your login name.

2. `Password :` It is your encrypted password. The password should be minimum 8-12 characters long including special characters, digits, lower case alphabetic and more. Usually password format is set to $id$salt$hashed, The $id is the algorithm used On GNU/Linux as follows:
    - $1$ is MD5
    - $2a$ is Blowfish
    - $2y$ is Blowfish
    - $5$ is SHA-256
    - $6$ is SHA-512

<br>

3. `Last password change (lastchanged) :` Days since Jan 1, 1970 that password was last changed.

4. `Minimum :` The minimum number of days required between password changes i.e. the number of days left before the user is allowed to change his/her password.

5. `Maximum :` The maximum number of days the password is valid (after that user is forced to change his/her password).

6. `Warn :` The number of days before password is to expire that user is warned that his/her password must be changed.

7. `Inactive :` The number of days after password expires that account is disabled

8. `Expire :` days since Jan 1, 1970 that account is disabled i.e. an absolute date specifying when the login may no longer be used.

## How do I change the Password
```bash
    $ passwd
```

## How do I change the password for other users?
```bash
    # passwd {username}
    $ sudo passwd {username}
```

[Next (GroupManagement)](groupmanagement.md)