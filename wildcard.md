## Some Wild Character Examples
```bash
    $ cp /tmp/a/* /tmp/b/
    $ cp /tmp/a/*.txt /tmp/b/
    $ cp /tmp/a/*.html /tmp/b/
```

Rename all .txt files to .bak
```bash
    $ mv *.txt *.bak
```

## Examples of escape character

```bash
    $ echo "Hello   \"World\""
    $ echo "A quote is \", backslash is \\, backtick is \`."
    $ echo "A few spaces are    and dollar is \$. \$X is ${X}."
    $ echo "This is \\ a backslash"
    $ echo "This is \" a quote and this is \\ a backslash"

```

[ Next (variables)](variables.md)