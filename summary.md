<table border="2">
<tbody>
   <tr>
      <th>Command</th>
      <th>Description</th>
      <th width="50%">Example</th>
   </tr>
   <tr>
      <th>&amp;</th>
      <td>Run the previous command in the background</td>
      <td><code>ls &amp;</code></td>
   </tr>
   <tr>
      <th>&amp;&amp;</th>
      <td>Logical AND</td>
      <td><code>if [ "$foo" -ge "0" ] &amp;&amp; [ "$foo" -le "9"]</code></td>
   </tr>
   <tr>
      <th>||</th>
      <td>Logical OR</td>
      <td><code>if [ "$foo" -lt "0" ] || [ "$foo" -gt "9" ]</code> (not in Bourne shell)</td>
   </tr>
   <tr>
      <th>^</th>
      <td>Start of line</td>
      <td><code>grep "^foo"</code></td>
   </tr>
   <tr>
      <th>$</th>
      <td>End of line</td>
      <td><code>grep "foo$"</code></td>
   </tr>
   <tr>
      <th>=</th>
      <td>String equality (cf. -eq)</td>
      <td><code>if [ "$foo" = "bar" ]</code></td>
   </tr>
   <tr>
      <th>!</th>
      <td>Logical NOT</td>
      <td><code>if [ "$foo" != "bar" ]</code></td>
   </tr>
   <tr>
      <th>$$</th>
      <td>PID of current shell</td>
      <td><code>echo "my PID = $$"</code></td>
   </tr>
   <tr>
      <th>$!</th>
      <td>PID of last background command</td>
      <td><code>ls &amp; echo "PID of ls = $!"</code></td>
   </tr>
   <tr>
      <th>$?</th>
      <td>exit status of last command</td>
      <td><code>ls ; echo "ls returned code $?"</code></td>
   </tr>
   <tr>
      <th>$0</th>
      <td>Name of current command (as called)</td>
      <td><code>echo "I am $0"</code></td>
   </tr>
   <tr>
      <th>$1</th>
      <td>Name of current command's first parameter</td>
      <td><code>echo "My first argument is $1"</code></td>
   </tr>
   <tr>
      <th>$9</th>
      <td>Name of current command's ninth parameter</td>
      <td><code>echo "My ninth argument is $9"</code></td>
   </tr>
   <tr>
      <th>$@</th>
      <td>All of current command's parameters (preserving whitespace and quoting)</td>
      <td><code>echo "My arguments are $@"</code></td>
   </tr>
   <tr>
      <th>$*</th>
      <td>All of current command's parameters (not preserving whitespace and quoting)</td>
      <td><code>echo "My arguments are $*"</code></td>
   </tr>
   <tr>
      <th>-eq</th>
      <td>Numeric Equality</td>
      <td><code>if [ "$foo" -eq "9" ]</code></td>
   </tr>
   <tr>
      <th>-ne</th>
      <td>Numeric Inquality</td>
      <td><code>if [ "$foo" -ne "9" ]</code></td>
   </tr>
   <tr>
      <th>-lt</th>
      <td>Less Than</td>
      <td><code>if [ "$foo" -lt "9" ]</code></td>
   </tr>
   <tr>
      <th>-le</th>
      <td>Less Than or Equal</td>
      <td><code>if [ "$foo" -le "9" ]</code></td>
   </tr>
   <tr>
      <th>-gt</th>
      <td>Greater Than</td>
      <td><code>if [ "$foo" -gt "9" ]</code></td>
   </tr>
   <tr>
      <th>-ge</th>
      <td>Greater Than or Equal</td>
      <td><code>if [ "$foo" -ge "9" ]</code></td>
   </tr>
   <tr>
      <th>-z</th>
      <td>String is zero length</td>
      <td><code>if [ -z "$foo" ]</code></td>
   </tr>
   <tr>
      <th>-n</th>
      <td>String is not zero length</td>
      <td><code>if [ -n "$foo" ]</code></td>
   </tr>
   <tr>
      <th>-nt</th>
      <td>Newer Than</td>
      <td><code>if [ "$file1" -nt "$file2" ]</code></td>
   </tr>
   <tr>
      <th>-d</th>
      <td>Is a Directory</td>
      <td><code>if [ -d /bin ]</code></td>
   </tr>