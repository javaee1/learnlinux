# Preset Variables

- **`$0`**: &nbsp;The variable $0 is the basename of the program as it was called.
- **`$1-$9`**: &nbsp; Are the first 9 additional parameters the script was called with.
- **`$@`**: &nbsp;The variable `$@` is all parameters `$1 .. whatever`.
- **`$*`**:&nbsp; The variable `$*`, is similar to `$@`, but does not preserve any whitespace, and quoting
- **`$#`**: &nbsp;is the number of parameters the script was called with.
- **`$$`**: &nbsp; The `$$` variable is the PID (Process IDentifier) of the currently running shell. 
- **`$!`**: &nbsp; The $! variable is the PID of the last run background process. 
- **`IFS`**:&nbsp; This is the Internal Field Separator. The default value is SPACE TAB NEWLINE, but if you are changing it, it's easier to take a copy;
- **`$?`**:&nbsp; This contains the exit value of the last run command.
**Note:  As a general rule, use $@ and avoid $*.**


## Backtick

The backtick is used to indicate that the enclosed text is to be executed as a command.
<br>
Remmember `shift Operator`.

[Read Here](https://www.shellscript.sh/variables2.html) more about varaibles.

[Next (IfCondition)](ifcondtion.md)
