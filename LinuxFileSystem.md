## Linux Directories

![linux-Directory-Structure](img/directory-structure.png)

## Linux FileSystem

![linux-Directory-Structure](img/standard-unix-filesystem-hierarchy.png)

[ClickHere](https://www.linux.com/training-tutorials/linux-filesystem-explained/) for detailed explaination.


## File Aceess Mode 

- `user` - Creater of file, symbol- `u`
- `group` - A set of user, symbol- `g`
- `other` - othet than user and group, symbol- `o`
-  **user + group + other = all => a**

## Let's Create a file
```bash
    cat > {filename}
```
- Also explain the symbol

## Unix File Permission
1. Symbolic Mode
    - `+` Adding Permission
    - `-` Removing Permission
    - `=` Assigning/Setting

2. Absolute Mode (Octal No System)
    - `1` => ON
    - `0` => OFF

```bash
    $ -rw-rw-r-- {no of hard links} {user} {group} {size of file in byte} {Create data} {file name}
```

**Absolute Mode (table)**
1. 0 - No Permission.
1. 1 - Execute.
1. 2 - write.
1. 3 - write and execute.
1. 4 - Read.
1. 5 - Read and Execute.
1. 6 - Read and Write.
1. 7 - Read,Write and execute.

## Sample Commands
```bash
$ chmod u-r {filename}
$ chmod u+r,u-w, g+w {filename}
$ chmod u-r {filename}
$ chmod u-r {filename}
$ chmod u+w, o+rwx {filename}
$ chmod u=w, o+rwx {filename}

$ chmod 400 {filename}
$ chmod 744 {filename}
```

[Next (UserManagement)](usermanagement.md)