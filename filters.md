# Introduction
Filters are programs that take plain text(either stored in a file or produced by another program) as standard input, transforms it into a meaningful format, and then returns it as standard output.

## List of Some Common Filters

- head
- tail
- nl
- cut
- paste
- sort
- tr (translation)
- tee 
- sed (stream editor)
- grep (Global Regular Expression Print)

**head** <br>
```bash
    $ head {filename} // by default display first ten
    $ head -{n} {filename} // display top n line
    $ head -{n} {filename1} {filename2} // display top n line
```

**tail** <br>
```bash
    $ tail {filename} // by default display last ten
    $ tail -{n} {filename} // display last n line
    $ tail -{n} {filename1} {filename2} // display last n line
```
**TIPS** <br>
```bash
    $ head -{n} {filename} | tail -{n}
```

**cut** <br>
**Remove sections from each line of files**
```bash
    $ cut -d' ' -f3 {file name}
    $ cut -d ' ' -f 2-4
    $ cut -c 11-20 {file name}
    $ cut -b 1,2,3 namelist // specific bytes
    $ cut -b 1-2,4-5 namelist // specific range of bytes
    
```

**paste** <br>

**sort** <br>

**tr** <br>
*Translate, squeeze, and/or delete characters from standard input, writing to standard output.*

```bash
    $ tr "\n" " " < {filename} // replace all new line to a space
    $ tr 'a-z' 'A-Z' < {filname} // convert lowercase to uppercase
    $ tr -d '0-9' < filename
```

**tee** <br>
*Copy standard input to each FILE, and also to standard output.*

```bash
    $ ll | tee {filename}
```

**sed** <br>
**Stream editor for filtering and transforming text**
```bash
    $ sed 's/version/story/g' myfile.txt  # Replace version with story in mytext.txt
    $ sed 's/version/story/gi' myfile.txt  # Replace version with story in mytext.txt (ignore case)
    $ sed '/^$/d' a.txt # delete blank line from a.txt

```


**grep** <br>
```bash
    $ grep {searchstring} {filenmae | [file list]}
    $ grep -i {searchkey} {filename} // ignore case
    $ grep -ni {searchkey} {filename} // also print line no
    $ grep -c {searchkey} {filename} // count no of mathces
    $ grep "u[mk]e" {filename}
    $ grep "^{key}" {filename} //starts with key
    $ grep "^[aeiou]" {filename}
    $ grep "^[^aeiou]" {filename} //negation
    $ grep "e$" {filename} // line end with e
    $ grep -c "^$" {filename} //count no of blank line
```

[Next (Wildcards)](wildcard.md)