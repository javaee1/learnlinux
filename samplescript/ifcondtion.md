# Introduction
**'[' is actually a program, just like ls and other programs, so it must be surrounded by spaces:**

```bash
    $ if [$foo = "bar" ]  # it won't work
    $ if SPACE [ SPACE "$foo" SPACE = SPACE "bar" SPACE ]
```

**The syntax for if...then...else... is:**
```
if [ ... ]
then
  # if-code
else
  # else-code
fi
```

```
if [ ... ]; then
  # do something
fi
```

**You can also use the elif, like this:**
```
if  [ something ]; then
 echo "Something"
 elif [ something_else ]; then
   echo "Something else"
 else
   echo "None of the above"
fi
```

```
if [ 10 -lt "15" ]; then echo texthere; fi
```
**Note: Some shells also accept "==" for string comparison; this is not portable, a single "=" should be used for strings, or "-eq" for integers.**

## Comparision literal
- -lt
- -gt
- -le
- -ge
- -n // non-zero length
- -nt file is newer than
- -ot file is older than
- -a, -e ( both meaning file exists)
