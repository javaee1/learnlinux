#!/bin/sh
for i in 1 2 3 4 5
do
  echo "Looping ... number $i"
done

# for i in hello 1 * 2 goodbye 
# do
#   echo "Looping ... i is set to $i"
# done

INPUT_STRING=hello
while [ "$INPUT_STRING" != "bye" ]  # why input_string in double quoute  , while : -> always true
do
  echo "Please type something in (bye to quit)"
  read INPUT_STRING
  echo "You typed: $INPUT_STRING"
done

for runlevel in 0 1 2 3 4 5 6 S
do
#   mkdir rc${runlevel}.d
echo "rc${runlevel}.d"
done

