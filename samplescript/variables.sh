#!/bin/sh
my_message="ntiesh"

my_custommessage=$my_message

echo $my_message
echo $my_custommessage


# Notes
# The shell does not care about types of variables; they may store strings, integers, real numbers - anything you like.
# There is no syntactic difference between:
# MY_MESSAGE="Hello World"
# MY_SHORT_MESSAGE=hi
# MY_NUMBER=1
# MY_PI=3.142
# MY_OTHER_PI="3.142"
# MY_MIXED=