# Group Manangement

## List all the existing group
```bash
    $ cat /etc/group
    $ getent group
```

## View groups associated with a user
```bash
    $ id {username}
    $ groups {username}
```
## Creating a group
```bash
    $ sudo groupadd {groupname}
```
## Deleting a group
```bash
    $ sudo groupdel {groupname}
```
## Understanding /etc/group File Format

![fileimage](./img/etc_group_file.webp) 
```
$ {group name}:{Password}:{GroupId}:{[Group list]}
```
1. `group_name:` It is the name of group. If you run ls -l command, you will see this name printed in the group field.
1. `Password:` Generally password is not used, hence it is empty/blank. It can store encrypted password. This is useful to implement privileged groups.
1. `Group ID (GID):` Each user must be assigned a group ID. You can see this number in your /etc/passwd file.
1. `Group List:` It is a list of user names of users who are members of the group. The user names, must be separated by commas.


[Next (Filters)](filters.md)